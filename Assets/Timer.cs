using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Timer : MonoBehaviour
{
    public float maxTime = 0;
    private float timeRemaining = 10;
    public bool timerIsRunning = false;
    public TextMeshProUGUI timerText;
    public UnityEvent OnTimerOff;

    private void Start()
    {
        // Starts the timer automatically
    }
    private void OnEnable()
    {
        ResetTimer();
        
    }

    void Update()
    {
        if (timerIsRunning)
        {
            DisplayTime(timeRemaining);
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                OnTimerOff.Invoke();
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;

            }
        }
    }
    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void ResetTimer()
    {
        timeRemaining = maxTime;
        timerIsRunning = true;
    }
}